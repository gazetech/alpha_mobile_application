package com.example.android.gazeapp;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anilburakbilsel on 2017-10-23.
 */

public class Search extends Fragment {


    private static final String TAG = "The Search";

    private StateManager state;
    private ArrayList<Bitmap> bitmapList;
    private GridView imageGrid;
    private ImageAdapter imgAdapter;

    Button btnSearch;
    EditText searchText;

    private void search() {

        final ArrayList<Bitmap> bitmapListFinal = bitmapList;
        final GridView imageGridFinal = imageGrid;
        final ImageAdapter imgAdapterFinal  = imgAdapter;

        if(state.get("access_token") != null) {

            String access_token = state.get("access_token").toString();


            API.getCommonObjects(access_token, searchText.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject obj) {

                    try {
                        boolean success = obj.getBoolean("success");
                        if (!success) {
                            return;
                        }

                        final JSONArray common_objects = obj.getJSONArray("common_objects");

                        new AsyncTask<Void, Void, Void>() {
                            protected void onPreExecute() {
                                // Pre Code
                            }

                            protected Void doInBackground(Void... unused) {
                                bitmapListFinal.clear();
                                for (int i = 0; i < common_objects.length(); i++) {
                                    JSONObject wb;
                                    try {
                                        wb = common_objects.getJSONObject(i);
                                        bitmapListFinal.add(Helpers.urlImageToBitmap(wb.getString("url")));
                                    } catch (Exception e) {
                                        continue;
                                    }
                                }

                                return new Void();
                            }

                            protected void onPostExecute(Void unused) {
                                imgAdapterFinal.setGridData(bitmapListFinal);
                            }
                        }.execute();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    return;
                }
            });
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        state = StateManager.getInstance();

        View view = inflater.inflate(R.layout.tab4_fragment,container,false);

        bitmapList = new ArrayList<>();
        imageGrid = (GridView) view.findViewById(R.id.searchGrid);
        imgAdapter = new ImageAdapter(getContext(), bitmapList);

        imageGrid.setAdapter(imgAdapter);

        final ArrayList<Bitmap> bitmapListFinal = bitmapList;
        final LayoutInflater infl = inflater;

        imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Bitmap image = bitmapListFinal.get(position);

                Dialog jpgDialog = new Dialog(getContext());
                jpgDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                jpgDialog.setContentView(infl.inflate(R.layout.jpgdialog
                        , null));
                jpgDialog.show();

                ImageView jpgDialogImage = (ImageView)jpgDialog.findViewById(R.id.image);
                jpgDialogImage.setImageBitmap(image);


            }
        });



        btnSearch = view.findViewById(R.id.btnSearch);
        searchText = view.findViewById(R.id.searchText);

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                search();
                return true;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });

        return view;
    }
}
