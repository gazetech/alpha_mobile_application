package com.example.android.gazeapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by anilburakbilsel on 2017-10-23.
 */

public class ListOfFaces extends Fragment {


    private static final String TAG = "List of Faces";


    private StateManager state;
    private ArrayList<Bitmap> bitmapList;
    private GridView imageGrid;
    private ImageAdapter imgAdapter;

    private void updateContent() {

        final ArrayList<Bitmap> bitmapListFinal = bitmapList;
        final GridView imageGridFinal = imageGrid;
        final ImageAdapter imgAdapterFinal  = imgAdapter;

        imageGrid.setAdapter(imgAdapter);

        if(state.get("access_token") != null) {
            String access_token = state.get("access_token").toString();

            API.getFaces(access_token, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject obj) {

                    try {
                        boolean success = obj.getBoolean("success");
                        if (!success) {
                            return;
                        }

                        final JSONArray faces = obj.getJSONArray("faces");

                        new AsyncTask<Void, Void, Void>() {
                            protected void onPreExecute() {
                                // Pre Code
                            }
                            protected Void doInBackground(Void... unused) {
                                bitmapListFinal.clear();
                                for (int i = 0; i < faces.length(); i++) {
                                    JSONObject face;
                                    try {
                                        face = faces.getJSONObject(i);
                                        bitmapListFinal.add(Helpers.urlImageToBitmap(face.getString("url")));
                                    } catch (Exception e) {
                                        continue;
                                    }
                                }

                                return new Void();
                            }
                            protected void onPostExecute(Void unused) {
                                imgAdapterFinal.setGridData(bitmapListFinal);
                            }
                        }.execute();



                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    return;
                }
            });
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        state = StateManager.getInstance();

        View view = inflater.inflate(R.layout.tab3_fragment,container,false);

        bitmapList = new ArrayList<>();
        imageGrid = (GridView) view.findViewById(R.id.gridview);
        imgAdapter = new ImageAdapter(getContext(), bitmapList);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateContent();
    }
}