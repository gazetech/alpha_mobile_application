package com.example.android.gazeapp;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by taus on 11/12/17.
 */

public class StateManager {
    private static Map<String, Object> state;
    private static StateManager singletonInstance = null;


    protected StateManager() {

    }

    public static StateManager getInstance() {
        if(singletonInstance == null) {
            singletonInstance = new StateManager();
            state = new HashMap<>();
            state.put("access_token", "59eda8c26209d12377fda416");
        }
        return singletonInstance;
    }

    public static void put(String key, Object value) {
        if (state == null) {
            state = new HashMap<>();
        }
        state.put(key, value);
    }

    public static Object get(String key) {
        if (state == null) {
            return null;
        }
        return state.get(key);
    }


}
