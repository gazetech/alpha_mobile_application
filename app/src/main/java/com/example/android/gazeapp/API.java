package com.example.android.gazeapp;

import android.content.Context;
import android.util.Log;

import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.FileInputStream;

import org.json.*;




/**
 * Created by taus on 11/9/17.
 */

public class API {
    private static String baseUrl = "http://10.0.0.147:5000";
    public static API singletonInstance = null;
    public static RequestQueue mRequestQueue = null;

    protected API(Context c) {
        mRequestQueue = getRequestQueue(c);
    }

    protected RequestQueue getRequestQueue(Context c) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(c.getApplicationContext());
        }
        return mRequestQueue;
    }

    public static API getInstance(Context c) {
        if(singletonInstance == null) {
            singletonInstance = new API(c);
        }
        return singletonInstance;
    }

    private static void sendRequest(int method, String url, final Response.Listener<JSONObject> successListener, final Response.ErrorListener errorListener, final Map<String, String> params, final Map<String, File> files) {
        Map<String, DataPart> filesMap = new HashMap<>();
        if (files != null) {
            for (String key : files.keySet()) {
                File file = files.get(key);

                InputStream is;
                try {
                    byte[] fileBytes = new byte[(int) file.length()];
                    is = new FileInputStream(file);
                    is.read(fileBytes);
                    DataPart dp = new DataPart(file.getName(), fileBytes);
                    filesMap.put(key, dp);

                } catch (Exception e) {
                    continue; // Unlikely to happen
                }
            }
        }

        final Map<String, DataPart> finalFilesMap = filesMap;

        Response.Listener<NetworkResponse> listenerToJson = new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(new String(response.data));
                } catch (Exception e) {
                    errorListener.onErrorResponse(new VolleyError("JSON from server could not be parsed"));
                }

                successListener.onResponse(jsonObj);
            }
        };

        VolleyMultipartRequest req = new VolleyMultipartRequest(method, url, listenerToJson, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                if (params == null)
                    return new HashMap<>();
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                if (finalFilesMap == null)
                    return new HashMap<>();

                return finalFilesMap;
            }
        };


        mRequestQueue.add(req);

    }

    public static void sendPictureFromDevice(String access_token, File picture, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String url = baseUrl + String.format("/picture/upload?access_token=%s", access_token) ;

        Map<String, File> files = new HashMap<>();
        files.put("file", picture);

        sendRequest(Request.Method.POST, url, successListener, errorListener, null, files);

    }


    public static void userLogin(String username, String password, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String url = baseUrl + "/user/login";

        Map<String, String> data = new HashMap<String, String>();
        data.put("username", username);
        data.put("password", password);

        sendRequest(Request.Method.POST, url, successListener, errorListener, data, null);
    }


    public static void userLoginFace(File faceImage, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String url = baseUrl + "/user/login/face";

        Map<String, File> files = new HashMap<>();
        files.put("file", faceImage);

        sendRequest(Request.Method.POST, url, successListener, errorListener, null, files);
    }


    public static void userRegister(String username, String password, File faceImage, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {

        String url = baseUrl + "/user/register";

        Map<String, File> files = new HashMap<>();
        files.put("file", faceImage);


        Map<String, String> data = new HashMap<String, String>();
        data.put("username", username);
        data.put("password", password);

        sendRequest(Request.Method.POST, url, successListener, errorListener, data, files);

    }

    public static void getWhiteboards(String access_token, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String url = baseUrl + String.format("/whiteboards?access_token=%s", access_token);

        sendRequest(Request.Method.GET, url, successListener, errorListener, null, null);
    }


    public static void getFaces(String access_token, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String url = baseUrl + String.format("/faces?access_token=%s", access_token);
        sendRequest(Request.Method.GET, url, successListener, errorListener, null, null);
    }

    public static void getCommonObjects(String access_token, String objectName, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        String url;
        try {
            url = baseUrl + String.format("/common_objects/search/%s?access_token=%s", URLEncoder.encode(objectName), access_token);
        } catch(Exception e) {
            errorListener.onErrorResponse(new VolleyError("URL couldn't be constructed"));
            return;
        }

        sendRequest(Request.Method.GET, url, successListener, errorListener, null, null);
    }

}
