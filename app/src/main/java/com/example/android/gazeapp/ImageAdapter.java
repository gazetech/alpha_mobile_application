package com.example.android.gazeapp;

/**
 * Created by taus on 11/12/17.
 */


import android.app.*;
import android.os.*;
import android.widget.*;
import java.util.*;
import android.graphics.*;
import android.view.*;
import android.content.*;

public class ImageAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Bitmap> bitmapList;

    private int numImagesPerRow = 5;

    public ImageAdapter(Context context, ArrayList<Bitmap> bitmapList) {
        this.context = context;
        this.bitmapList = bitmapList;
    }

    public void setGridData(ArrayList<Bitmap> mGridData) {
        this.bitmapList = mGridData;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.bitmapList.size();
    }
    public Object getItem(int position) {
        return null;
    }
    public long getItemId(int position) {
        return 0;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        int pixels = (int) ((((float) (this.context.getResources().getDisplayMetrics().widthPixels - ((int) (4 * ((float) this.context.getResources().getDisplayMetrics().densityDpi / this.context.getResources().getDisplayMetrics().DENSITY_DEFAULT )))))) / (float)numImagesPerRow);


        if (convertView == null) {
            imageView = new ImageView(this.context);

            imageView.setLayoutParams(new GridView.LayoutParams(pixels, pixels));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageBitmap(this.bitmapList.get(position));
        return imageView;
    }
}